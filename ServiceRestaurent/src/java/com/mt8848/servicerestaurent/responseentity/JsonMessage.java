/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.responseentity;

import java.util.List;

/**
 *
 * @author mansubh
 */
public class JsonMessage<T> {
    private String status;
    private String message;
    private List<T> objectlist;

    public JsonMessage() {
    }

    
    public JsonMessage(String status, String message, List objectlist) {
        this.status = status;
        this.message = message;
        this.objectlist = objectlist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getObjectlist() {
        return objectlist;
    }

    public void setObjectlist(List<T> objectlist) {
        this.objectlist = objectlist;
    }

   
    
}
