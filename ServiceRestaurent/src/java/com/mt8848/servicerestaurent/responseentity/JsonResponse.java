/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.responseentity;

/**
 *
 * @author mansubh
 */
public class JsonResponse {
    private String status;
    private String message;
    private int response_code;

    public JsonResponse() {
    }

    
    public JsonResponse(String status, String message, int response_code) {
        this.status = status;
        this.message = message;
        this.response_code = response_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }


    
}
