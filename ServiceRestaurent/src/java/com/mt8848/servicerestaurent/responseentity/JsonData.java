/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.responseentity;

/**
 *
 * @author mansubh
 */
public class JsonData<T> {
    private String status;
    private String message;
    private T data;

    public JsonData() {
    }

    public JsonData(String status, String message, T data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    
    public String getStatus() {
        return status;
    }
    
    

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
    
    
}
