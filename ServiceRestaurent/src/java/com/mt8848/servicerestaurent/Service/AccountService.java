/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.Service;

import com.mt8848.servicerestaurent.entity.Account;
import java.util.List;

/**
 *
 * @author mansubh
 */
public interface AccountService {
    
     int insert(Account a);
    int update(Account a);
    int delete(int id);
    Account getById(int id);
    Account getbyphonenumber(String phonenumber);
    List<Account> getAll();
}
