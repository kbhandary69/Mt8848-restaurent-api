/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.Service;

import com.mt8848.servicerestaurent.entity.Menu;
import java.util.List;

/**
 *
 * @author mansubh
 */
public interface MenuService {
    
    void insert (Menu m);
void update (Menu m);
List<Menu> getAll();
Menu getByID(int id);
List<Menu> getByCategory(int id);
}
