/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.Service;

import com.mt8848.servicerestaurent.entity.Category;
import java.util.List;

/**
 *
 * @author mansubh
 */
public interface CategoryService {
      Category getById(int id);
    List<Category> getAll();
}
