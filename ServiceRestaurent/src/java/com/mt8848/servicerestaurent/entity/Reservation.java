/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mansubh
 */
@Entity
@Table(name = "tbl_reservation")
public class Reservation {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reservation_id")
    private int reservation_id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @Column(name = "access_token")
    private String access_token;
    @Column(name ="reservation_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reservation_time;
    @Column(name = "reservation_status")
    private String reservation_status;
    @Column(name = "reservation_table_count")
    private int reservation_table_count;
    @Column(name = "num_of_people")
    private int num_of_people;

    public Reservation() {
    }

    public Reservation(int reservation_id, String access_token, String reservation_status) {
        this.reservation_id = reservation_id;
        this.access_token = access_token;
        this.reservation_status = reservation_status;
    }

    public int getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(int reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }


    public String getReservation_status() {
        return reservation_status;
    }

    public void setReservation_status(String reservation_status) {
        this.reservation_status = reservation_status;
    }

    public int getReservation_table_count() {
        return reservation_table_count;
    }

    public void setReservation_table_count(int reservation_table_count) {
        this.reservation_table_count = reservation_table_count;
    }

    public int getNum_of_people() {
        return num_of_people;
    }

    public void setNum_of_people(int num_of_people) {
        this.num_of_people = num_of_people;
    }

    public Date getReservation_time() {
        return reservation_time;
    }

    public void setReservation_time(Date reservation_time) {
        this.reservation_time = reservation_time;
    }

    
    
    
    
}
