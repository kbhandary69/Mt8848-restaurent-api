/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.entity;

/**
 *
 * @author mansubh
 */
public class ReservationBody {

    private String access_token;
    private String reservation_time;
    private int reservation_table_count;
    private int num_of_people;

    public ReservationBody() {
    }

    public ReservationBody(String access_token, String reservation_time) {

        this.access_token = access_token;
        this.reservation_time = reservation_time;

    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getReservation_time() {
        return reservation_time;
    }

    public void setReservation_time(String reservation_time) {
        this.reservation_time = reservation_time;
    }

    public int getReservation_table_count() {
        return reservation_table_count;
    }

    public void setReservation_table_count(int reservation_table_count) {
        this.reservation_table_count = reservation_table_count;
    }

    public int getNum_of_people() {
        return num_of_people;
    }

    public void setNum_of_people(int num_of_people) {
        this.num_of_people = num_of_people;
    }

}
