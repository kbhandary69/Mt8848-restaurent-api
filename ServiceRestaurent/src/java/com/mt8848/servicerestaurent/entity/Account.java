/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mansubh
 */
@Entity
@Table(name = "tbl_account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private int account_id;
    @Column(name = "full_name")
    private String full_name;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phone_number;
    @Column(name= "password")
    private String password;
    @Column(name = "creation_device")
    private String creation_device;
    @Column(name = "creation_time",insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date creation_time;
    @Column(name = "account_status")
    private boolean account_status;
   

    public Account() {
    }

    public Account(int account_id, String full_name, String phone_number, String password) {
        this.account_id = account_id;
        this.full_name = full_name;
        this.phone_number = phone_number;
        this.password = password;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCreation_device() {
        return creation_device;
    }

    public void setCreation_device(String creation_device) {
        this.creation_device = creation_device;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }

    public boolean isAccount_status() {
        return account_status;
    }

    public void setAccount_status(boolean account_status) {
        this.account_status = account_status;
    }

    @Override
    public String toString() {
        return "Account{" + "account_id=" + account_id + ", full_name=" + full_name + ", email=" + email + ", phone_number=" + phone_number + ", creation_device=" + creation_device + ", password=" + password + ", creation_time=" + creation_time + ", account_status=" + account_status + '}';
    }

  
    
    
    
}
