/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author mansubh
 */
@Entity
@Table(name = "tbl_login")
public class Login {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="login_id")
    private int login_id;
    @Column(name="login_time",insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date login_time;
    @Column(name="login_token")
    private String login_token;
    @Column(name = "login_token_status")
    private boolean login_token_status;
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    public Login() {
    }

    public Login(int login_id) {
        this.login_id = login_id;
    }

    public int getLogin_id() {
        return login_id;
    }

    public void setLogin_id(int login_id) {
        this.login_id = login_id;
    }

    public Date getLogin_time() {
        return login_time;
    }

    public void setLogin_time(Date login_time) {
        this.login_time = login_time;
    }

    public String getLogin_token() {
        return login_token;
    }

    public void setLogin_token(String login_token) {
        this.login_token = login_token;
    }

    public boolean isLogin_token_status() {
        return login_token_status;
    }

    public void setLogin_token_status(boolean login_token_status) {
        this.login_token_status = login_token_status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    
}
