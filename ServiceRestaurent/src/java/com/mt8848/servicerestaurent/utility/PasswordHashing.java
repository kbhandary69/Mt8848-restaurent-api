/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author mansubh
 */
public class PasswordHashing {
    
    public static String getpasswordhash(String password, String date){
         String generatepassword = null;
        try{
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(date.getBytes("UTF-8"));
            byte[] bytes = md.digest(password.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<bytes.length ; i++){
                sb.append(Integer.toString((bytes[i] & 0Xff) + 0x100,16).substring(1));
            }
            generatepassword = sb.toString();
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e){
            System.out.println(e.getMessage());
        }
        
        return  generatepassword;
    }
}
