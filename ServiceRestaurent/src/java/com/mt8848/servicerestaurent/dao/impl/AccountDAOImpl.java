/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.AccountDAO;
import com.mt8848.servicerestaurent.entity.Account;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class AccountDAOImpl implements AccountDAO{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    
    
    @Override
    public int insert(Account a) {
    session = sessionFactory.openSession();
    transaction = session.beginTransaction();
    session.save(a);
    transaction.commit();
    session.close();
    return 1;
    }

    @Override
    public int update(Account a) {
    session = sessionFactory.openSession();
    transaction = session.beginTransaction();
    session.saveOrUpdate(a);
    transaction.commit();
    session.close();
    return 1;
    }

    @Override
    public int delete(int id) {
  session = sessionFactory.openSession();
    transaction = session.beginTransaction();
    Account a =(Account)session.get(Account.class, id);
    session.delete(a);
    transaction.commit();
    session.close(); 
    return 1;
    }

    @Override
    public Account getById(int id) {
    session = sessionFactory.openSession();
    Account a = (Account)session.get(Account.class, id);
    session.close();
    return a;
    }

    @Override
    public Account getbyphonenumber(String phonenumber) {
    for(Account a : getAll()){
        if(a.getPhone_number().equals(phonenumber)){
            return a;
        }
    }
    return null;
    }

    @Override
    public List<Account> getAll() {
    session = sessionFactory.openSession();
    List<Account> accountlist = session.createQuery("SELECT a from Account a").list();
    session.close();
    return accountlist;
    }
    
}
