/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.LoginDAO;
import com.mt8848.servicerestaurent.entity.Login;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class LoginDAOImpl implements LoginDAO{
    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    @Override
    public int insert(Login l) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
        session.save(l);
        transaction.commit();
        session.close();
        return 1;     
    }

    @Override
    public List<Login> getAll() {
    session = sessionFactory.openSession();
    List<Login> loginlist = session.createQuery("SELECT l from Login l").list();
    session.close();
    return loginlist;
    }

    @Override
    public Login getByAccessToken(String accesstoken) {
    for(Login l : getAll()){
    if(l.getLogin_token().equals(accesstoken)){
        return l;
    }
    }
    return null;    
    }
    
    
}
