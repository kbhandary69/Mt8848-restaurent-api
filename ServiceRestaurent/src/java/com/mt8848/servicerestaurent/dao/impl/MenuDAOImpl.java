/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.MenuDAO;
import com.mt8848.servicerestaurent.entity.Menu;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class MenuDAOImpl implements MenuDAO {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;

    @Override
    public void insert(Menu m) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
        session.save(m);
        transaction.commit();
        session.close();
    }
    

    @Override
    public List<Menu> getAll() {
        session = sessionFactory.openSession();
        List<Menu> menulist = session.createQuery("SELECT m from Menu m").list();
        session.close();
        return menulist;
    }

    @Override
    public Menu getByID(int id) {
        session = sessionFactory.openSession();
        Menu m = (Menu) session.get(Menu.class, id);
        session.close();
        return m;
    }

    @Override
    public List<Menu> getByCategory(int id) {
        List<Menu> mlist= new ArrayList<>();
       for(Menu m : getAll()){
           if(m.getCategory().getCategory_id() == id){
               mlist.add(m);
           }
       }
       return mlist;
    }

    @Override
    public void update(Menu m) {
    session = sessionFactory.openSession();
     transaction = session.beginTransaction();
     session.saveOrUpdate(m);
     transaction.commit();
     session.close();    
    }

    

}
