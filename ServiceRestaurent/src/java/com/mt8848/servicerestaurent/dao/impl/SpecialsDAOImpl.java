/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.SpecialsDAO;
import com.mt8848.servicerestaurent.entity.Specials;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class SpecialsDAOImpl implements SpecialsDAO{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    @Override
    public List<Specials> getAll() {
        session = sessionFactory.openSession();
        List<Specials> slist = session.createQuery("SELECT s from Specials s").list();
        session.close();
        return slist;
    }
    
    
    
}
