/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.CategoryDAO;
import com.mt8848.servicerestaurent.entity.Category;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class CategoryDAOImpl implements CategoryDAO{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    @Override
    public Category getById(int id) {
        session = sessionFactory.openSession();
       Category c = (Category)session.get(Category.class, id);
       session.close();
       return c;
    }

    @Override
    public List<Category> getAll() {
        session = sessionFactory.openSession();
        List<Category> categoryList = session.createQuery("Select c from Category c").list();
        session.close();
        return categoryList;
    }
    
    
    
    
    
}
