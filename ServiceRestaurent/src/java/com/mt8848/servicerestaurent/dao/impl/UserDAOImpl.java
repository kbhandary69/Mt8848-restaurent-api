/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.UserDAO;
import com.mt8848.servicerestaurent.entity.User;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class UserDAOImpl implements UserDAO{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    @Override
    public int insert(User u) {
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
        session.save(u);
        transaction.commit();
        session.close();
       return 1;
    }

    @Override
    public List<User> getAll() {
    session = sessionFactory.openSession();
    List<User> userlist = session.createQuery("SELECT u from User u").list();
    session.close();
    return userlist;
    }

    @Override
    public User getByphonenumber(String phonenumber) {
       for(User u : getAll()){
           if(u.getPhone_number().equals(phonenumber)){
           return u;    
           }
       }
       return null;
    }
    
    
}
