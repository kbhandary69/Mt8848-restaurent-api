/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.dao.impl;

import com.mt8848.servicerestaurent.dao.ReservationDAO;
import com.mt8848.servicerestaurent.entity.Reservation;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mansubh
 */
@Repository
public class ReservationDAOImpl implements ReservationDAO{

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    
    @Override
    public int insert(Reservation r) {
    session = sessionFactory.openSession();
    transaction = session.beginTransaction();
    session.save(r);
    transaction.commit();
    session.close();
    return 1;
    }

    @Override
    public int update(Reservation r) {
    session = sessionFactory.openSession();
    transaction = session.beginTransaction();
    session.saveOrUpdate(r);
    transaction.commit();
    session.close();
    return 1;
    }

    @Override
    public List<Reservation> getAll() {
        session= sessionFactory.openSession();
        List<Reservation> reservationlist = session.createQuery("SELECT r from Reservation r").list();
        session.close();
        return reservationlist;
    }


    @Override
    public Reservation getByAccessToken(String accesstoken) {
    for(Reservation r : getAll()){
        if(r.getAccess_token().equals(accesstoken)){
            return r;
        }
    }
    return null;
    }
    
    
    
}
