/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.SpecialsService;
import com.mt8848.servicerestaurent.responseentity.JsonMessage;
import com.mt8848.servicerestaurent.entity.Specials;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mansubh
 */
@RestController
public class SpecialsController {
    
    @Autowired
    private SpecialsService specialsService;
    
   @RequestMapping(value = "/api/specials/",method = RequestMethod.GET)
   public ResponseEntity<JsonMessage> getSpecials(){
       JsonMessage message = new JsonMessage();
       
       List<Specials> slist = specialsService.getAll();
       if(slist.isEmpty()){
           return new ResponseEntity<>(HttpStatus.NO_CONTENT);
       }else{
           message.setStatus("200 OK");
           message.setMessage("Success");
           message.setObjectlist(slist);
           return new ResponseEntity<>(message,HttpStatus.OK);
       }
       
   } 
    
}
