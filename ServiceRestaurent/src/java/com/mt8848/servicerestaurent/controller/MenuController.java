/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.MenuService;
import com.mt8848.servicerestaurent.entity.Category;
import com.mt8848.servicerestaurent.responseentity.JsonData;
import com.mt8848.servicerestaurent.responseentity.JsonMessage;
import com.mt8848.servicerestaurent.entity.Menu;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author mansubh
 */
@RestController
@RequestMapping(value = "/")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Menu> index() {
        List<Menu> mlist = menuService.getAll();
        return mlist;
    }

    //getting all the data drom the menu database
    @RequestMapping(value = "/api/menu/all", method = RequestMethod.GET)
    public ResponseEntity<JsonMessage> getData() {
        JsonMessage message = new JsonMessage();

        List<Menu> mlist = menuService.getAll();
        if (mlist.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            message.setStatus("200 OK");
            message.setMessage("Success");
            message.setObjectlist(mlist);
            return new ResponseEntity<>(message, HttpStatus.OK);
        }
    }

    //retrieving single user
    @RequestMapping(value = "api/menu/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JsonData> getSingleData(@PathVariable("id") int id) {
        JsonData data = new JsonData();
        Menu menu = menuService.getByID(id);
        if (menu == null) {
            System.out.println("User with id" + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            data.setStatus("200 OK");
            data.setMessage("success");
            data.setData(menu);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }

    //creating a new menu by post method
    @RequestMapping(value = "api/menu/", method = RequestMethod.POST)
    public ResponseEntity<Void> createMenu(@RequestBody Menu menu, UriComponentsBuilder ucBuilder) {
        System.out.println("inserting menu:" + menu.getMenu_name());

        //validation work
        menuService.insert(menu);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("api/menu/{id}").buildAndExpand(menu.getMenu_id()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    //updating a menu object
    @RequestMapping(value = "api/menu/update/{id}", method = RequestMethod.POST)
    public ResponseEntity<JsonData> updateMenu(@PathVariable("id") int id, @RequestBody Menu menu, UriComponentsBuilder ucbuilder) {
        System.out.println("updating menu: " + menu.getMenu_name());
        JsonData data = new JsonData();
        //validation work
        Menu m = menuService.getByID(id);
        if (m == null) {
            System.out.println("menu with id : " + id + "not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else { 
            m.setMenu_name(menu.getMenu_name());
            m.setPrice(menu.getPrice());
            m.setCategory(new Category(menu.getCategory().getCategory_id() , menu.getCategory().getCategory_name()));
            menuService.update(m);
            data.setStatus("200 OK");
            data.setMessage("sucess");
            data.setData(m);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }
}
