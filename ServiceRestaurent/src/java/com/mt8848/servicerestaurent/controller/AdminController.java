/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.UserService;
import com.mt8848.servicerestaurent.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author mansubh
 */
@Controller
@RequestMapping(value = "/")
public class AdminController {
    
    @Autowired
    UserService userService;
    
    @RequestMapping(value = "/")
    public void insertAdmin(){
        userService.insert(new User(0, "admin123", "123456"));
    }
    
}
