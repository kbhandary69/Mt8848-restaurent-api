/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.CategoryService;
import com.mt8848.servicerestaurent.Service.MenuService;
import com.mt8848.servicerestaurent.entity.Category;
import com.mt8848.servicerestaurent.responseentity.JsonMessage;
import com.mt8848.servicerestaurent.entity.Menu;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mansubh
 */
@RestController
public class CategoryController {
    
    @Autowired
    private MenuService menuService;
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping(value = "api/category/", method = RequestMethod.GET)
    public ResponseEntity<JsonMessage> getCategory(){
        
          JsonMessage message = new JsonMessage();
        
         List<Category> clist = categoryService.getAll();
         if(clist.isEmpty()){
             return new ResponseEntity<>(HttpStatus.NO_CONTENT);
         }else{
          message.setStatus("200 OK");
             message.setMessage("Success");
             message.setObjectlist(clist);
         return new ResponseEntity<>(message,HttpStatus.OK);
         }
    }
    
    @RequestMapping(value = "api/category/{id}",method = RequestMethod.GET)
    public ResponseEntity<JsonMessage> getCategoryById(@PathVariable("id") int id){
        
        JsonMessage message = new JsonMessage();
        
        List<Menu> menulist = menuService.getByCategory(id);
        if(menulist.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            
        }
        else{
            message.setStatus("200 OK");
            message.setMessage("Success");
            message.setObjectlist(menulist);
            return new ResponseEntity<>(message,HttpStatus.OK);
        }
    }
    
    
   
    
}
