/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.LoginService;
import com.mt8848.servicerestaurent.Service.ReservationService;
import com.mt8848.servicerestaurent.entity.Login;
import com.mt8848.servicerestaurent.entity.Reservation;
import com.mt8848.servicerestaurent.entity.ReservationBody;
import com.mt8848.servicerestaurent.responseentity.JsonResponse;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mansubh
 */
@RestController
@RequestMapping(value = "/")
public class ReservationController {

    private static final String RESERVATION_STATUS_ACTIVE = "active";
    private static final String RESERVATION_STATUS_COMPLETED = "completed";
    private static final String RESERVATION_STATUS_CANCELLED = "cancelled";

    @Autowired
    private ReservationService reservationService;
    @Autowired
    private LoginService loginService;

    //making a reservation 
    //client sends a reservation post request
    @RequestMapping(value = "api/reservation", method = RequestMethod.POST)
    public ResponseEntity<JsonResponse> doReservation(@RequestBody ReservationBody reservationbody) {

        JsonResponse response = new JsonResponse();
        String accesstoken = reservationbody.getAccess_token();
        String dateinString = reservationbody.getReservation_time();
        System.out.println("sentdate : "+ dateinString);
        Date date = new Date();
        System.out.println("currentdate: " + date.toString());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Date datefromrequest = sdf.parse(dateinString);
            System.out.println("sent date after parsed:" + datefromrequest.toString());
            int datecheckparam = datefromrequest.compareTo(date);
            System.out.println("datecheckparam = " + datecheckparam);

            for (Login l : loginService.getall()) {
                if ((l.getLogin_token().equals(accesstoken)) && (datecheckparam > 0)) {
                    if(reservationService.getByAccessToken(accesstoken).getReservation_status().equals(RESERVATION_STATUS_ACTIVE)){
                        response.setStatus("error");
                        response.setMessage("already active reservation..cancel previous one to make new reservation");
                        response.setResponse_code(5);
                        return new ResponseEntity<>(response,HttpStatus.NOT_ACCEPTABLE);
                    }else{
                    Reservation reservation = new Reservation();

                    reservation.setAccess_token(accesstoken);
                    reservation.setReservation_status(RESERVATION_STATUS_ACTIVE);
                    Login login = loginService.getByAccessToken(accesstoken);
                    reservation.setAccount(login.getAccount());

                    java.sql.Timestamp sqdate = new java.sql.Timestamp(datefromrequest.getTime());

                    reservation.setReservation_time(sqdate);
                    reservation.setNum_of_people(reservationbody.getNum_of_people());
                    reservation.setReservation_table_count(reservationbody.getReservation_table_count());
                    reservationService.insert(reservation);

                    response.setStatus("success");
                    response.setMessage("reservation successfully done");
                    response.setResponse_code(0);
                    return new ResponseEntity<>(response, HttpStatus.CREATED);
                    }
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.setStatus("error");
        response.setMessage("reservation failed..incorrect date or accesstoken");
        response.setResponse_code(4);
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);

    }
}
