/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.controller;

import com.mt8848.servicerestaurent.Service.AccountService;
import com.mt8848.servicerestaurent.Service.LoginService;
import com.mt8848.servicerestaurent.Service.UserService;
import com.mt8848.servicerestaurent.entity.Account;
import com.mt8848.servicerestaurent.responseentity.JsonData;
import com.mt8848.servicerestaurent.responseentity.JsonResponse;
import com.mt8848.servicerestaurent.entity.Login;
import com.mt8848.servicerestaurent.entity.User;
import com.mt8848.servicerestaurent.responseentity.LoginResponse;
import com.mt8848.servicerestaurent.utility.PasswordHashing;
import com.mt8848.servicerestaurent.utility.TokenGeneration;
import java.sql.Timestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author mansubh
 */
@Controller
@RequestMapping(value = "/")
public class LoginController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private UserService userService;
    @Autowired
    private LoginService loginService;

    //creting a new user for the(Signup)
    @RequestMapping(value = "api/signup", method = RequestMethod.POST)
    public ResponseEntity<JsonResponse> createAccount(@RequestBody Account account) {
        System.out.println("creating account...");
        JsonData data = new JsonData();
        JsonResponse response = new JsonResponse();

        //check if the phone number and email are dublicate
        for (Account a : accountService.getAll()) {
            if (account.getEmail().equals(a.getEmail()) && (account.getPhone_number().equals(a.getPhone_number()))) {
                response.setStatus("error");
                response.setMessage("duplicate phonenumber or email");
                response.setResponse_code(2);
                return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
            }
        }

        String password = account.getPassword();
        String phnumber = account.getPhone_number();
        String encryptedPassword = PasswordHashing.getpasswordhash(password, phnumber);
        // System.out.println(encryptedPassword);
        account.setPassword(encryptedPassword);
        //inserting into data base after password hashing
        int checkinsert = accountService.insert(account);
        if (checkinsert == 1) {
            User user = new User();
            user.setPhone_number(account.getPhone_number());
            user.setPassword(account.getPassword());
            user.setAccount(accountService.getbyphonenumber(account.getPhone_number()));
            int checkinsert2 = userService.insert(user);
            if (checkinsert2 == 1) {
                response.setStatus("success");
                response.setMessage("inserted successfully");
                response.setResponse_code(0);
                return new ResponseEntity<>(response, HttpStatus.CREATED);
            }
        }

        response.setStatus("error");
        response.setMessage("unable to insert.. Server error");
        response.setResponse_code(2);
        return new ResponseEntity<>(response, HttpStatus.SERVICE_UNAVAILABLE);

    }

    //login work
    @RequestMapping(value = "api/login", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> doLogin(@RequestBody User user) {

        LoginResponse response = new LoginResponse();
         
        String Hashedpassword = PasswordHashing.getpasswordhash(user.getPassword(), user.getPhone_number());

        for (User u : userService.getAll()) {

            //username password validation
            if ((u.getPhone_number().equals(user.getPhone_number())) && (u.getPassword().equals(Hashedpassword))) {
                Login login = new Login();
                String currenttime = new Timestamp(System.currentTimeMillis()).toString();
                String datatohash = user.getPassword() + currenttime;
                String access_token = TokenGeneration.getrandomtoken(datatohash);
                login.setLogin_token(access_token);
                login.setLogin_token_status(true);
                login.setAccount(accountService.getbyphonenumber(user.getPhone_number()));

                loginService.insert(login);

                response.setStatus("success");
                response.setMessage("logged in successfully");
                response.setResponse_code(0);
                User u1 = userService.getByphonenumber(user.getPhone_number());
                System.out.println(u1.getUser_id());
                response.setUser_id(u1.getUser_id());
                
                response.setAccess_token(access_token);
                return new ResponseEntity<>(response, HttpStatus.OK);

                //login.setAccount(new Account(user., full_name, phone_number, password));
            }
        }
        response.setStatus("error");
        response.setMessage("invalid username or password");
        response.setResponse_code(1);
        response.setAccess_token(null);
        response.setUser_id(0);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

}
