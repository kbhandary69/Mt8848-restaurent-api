/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.AccountService;
import com.mt8848.servicerestaurent.dao.AccountDAO;
import com.mt8848.servicerestaurent.entity.Account;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    AccountDAO accountDao;
    
    @Override
    public int insert(Account a) {
    return accountDao.insert(a);
    }

    @Override
    public int update(Account a) {
    return accountDao.update(a);
    }

    @Override
    public int delete(int id) {
    return accountDao.delete(id);
    }

    @Override
    public Account getById(int id) {
    return accountDao.getById(id);
    }

    @Override
    public Account getbyphonenumber(String phonenumber) {
    return accountDao.getbyphonenumber(phonenumber);
    }

    @Override
    public List<Account> getAll() {
    return accountDao.getAll();
    }
    
    
    
}
