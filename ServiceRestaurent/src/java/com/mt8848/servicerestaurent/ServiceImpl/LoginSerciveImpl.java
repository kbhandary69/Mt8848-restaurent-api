/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.LoginService;
import com.mt8848.servicerestaurent.dao.LoginDAO;
import com.mt8848.servicerestaurent.entity.Login;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service
public class LoginSerciveImpl implements LoginService{

    @Autowired
    LoginDAO loginDao;
    
    @Override
    public int insert(Login l) {
    loginDao.insert(l);
    return 1;
    }

    @Override
    public List<Login> getall() {
    return loginDao.getAll();
    }

    @Override
    public Login getByAccessToken(String accesstoken) {
    return loginDao.getByAccessToken(accesstoken);
    }
    
    
    
}
