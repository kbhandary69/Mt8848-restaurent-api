/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.UserService;
import com.mt8848.servicerestaurent.dao.UserDAO;
import com.mt8848.servicerestaurent.entity.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserDAO userDao;
    
    @Override
    public int insert(User u) {
    return userDao.insert(u);
    }

    @Override
    public List<User> getAll() {
    return userDao.getAll();
    }

  

    @Override
    public User getByphonenumber(String phonenumber) {
    return userDao.getByphonenumber(phonenumber);
    }
    
    
    
}
