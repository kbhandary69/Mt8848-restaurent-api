/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.MenuService;
import com.mt8848.servicerestaurent.dao.MenuDAO;
import com.mt8848.servicerestaurent.entity.Menu;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service()
public class MenuServiceImpl implements MenuService{

    @Autowired
    private MenuDAO menuDao;
    
    @Override
    public void insert(Menu m) {
    menuDao.insert(m);
    }

    @Override
    public void update(Menu m) {
    menuDao.update(m);
    }

    @Override
    public List<Menu> getAll() {
   return menuDao.getAll();
    }

    @Override
    public Menu getByID(int id) {
   return menuDao.getByID(id);
    }

    @Override
    public List<Menu> getByCategory(int id) {
    return menuDao.getByCategory(id);
    }
    
    
    
}
