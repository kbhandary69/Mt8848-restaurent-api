/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.ReservationService;
import com.mt8848.servicerestaurent.dao.ReservationDAO;
import com.mt8848.servicerestaurent.entity.Reservation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service
public class ReservationServiceImpl implements ReservationService{

    @Autowired
    ReservationDAO reservationDao;
    @Override
    public int insert(Reservation r) {
    return reservationDao.insert(r);
    }

    @Override
    public int update(Reservation r) {
    return reservationDao.update(r);
    }

    @Override
    public List<Reservation> getAll() {
    return reservationDao.getAll();
    }

    @Override
    public Reservation getByAccessToken(String accesstoken) {
    return reservationDao.getByAccessToken(accesstoken);
    }
    
    
    
}
