/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mt8848.servicerestaurent.ServiceImpl;

import com.mt8848.servicerestaurent.Service.SpecialsService;
import com.mt8848.servicerestaurent.dao.SpecialsDAO;
import com.mt8848.servicerestaurent.entity.Specials;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mansubh
 */
@Service
public class SpecialsServiceImpl implements SpecialsService{

    @Autowired
    SpecialsDAO specialsDao;
    
    @Override
    public List<Specials> getAll() {
    return specialsDao.getAll();
    }

  
    
    
}
